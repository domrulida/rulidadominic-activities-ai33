(function($) {

	"use strict";

	var fullHeight = function() {

		$('.js-fullheight').css('height', $(window).height());
		$(window).resize(function(){
			$('.js-fullheight').css('height', $(window).height());
		});

	};
	fullHeight();

	$('#sidebarCollapse').on('click', function () {
      $('#sidebar').toggleClass('active');
  });

})(jQuery);



                                      window.onload = function() {
                                          new Chart(document.getElementById('chart-area').getContext('2d'), pieChartData);

                                          new Chart(document.getElementById('canvas').getContext('2d'), {
                                                  type: 'bar',
                                                  data: barChartData,
                                                  options: {
                                                          title: {
                                                                  display: true,
                                                                  text: 'Borrowed and Returned Books'
                                                          },
                                                          tooltips: {
                                                                  mode: 'index',
                                                                  intersect: false
                                                          },
                                                          responsive: true,
                                                          scales: {
                                                                  xAxes: [{
                                                                          stacked: true,
                                                                  }],
                                                                  yAxes: [{
                                                                          stacked: true
                                                                  }]
                                                          }
                                                  }
                                          });
                                      }

                                 




