import Vue from "vue";
import VueRouter from "vue-router";
import Dashboard from "../Dashboard.vue";
import Patron from "../Patron.vue";
import Book from "..//Books.vue";
import Settings from "../Settings.vue";

Vue.use(VueRouter);

export default new VueRouter({
  mode: "history",
  routes: [
    {
      path: "/",
      component: Dashboard,
    },
    {
      path: "/Dashboard",
      name: "Dashboard",
      component: Dashboard,
    },
    {
      path: "/Patron",
      name: "Patron",
      component: Patron,
    },

   {
      path: "/Book",
      name: "Book",
      component: Book,
    },
    
    {
      path: "/Settings",
      name: "Settings",
      component: Settings,
    },
    
  ],
});
